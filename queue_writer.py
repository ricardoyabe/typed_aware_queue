import json
import argparse

from alana.domain import Parent, Child
from alana.queue import RedisQueue, TypeAwareQueue

q = RedisQueue(name='queue', host='127.0.0.1', port=6379)
taq = TypeAwareQueue(q)
taq.add_encoder_decoder(
    clazz=Parent,
    encoder_callback=lambda obj: obj.json(),
    decoder_callback=lambda str_rep: Parent(**json.loads(str_rep))
)


def main(name: str, age: int) -> None:
    child = Child(name='child name', age=3)
    parent = Parent(name=name, age=age, child=[child])
    print(parent)
    taq.rpush(parent)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Redis queue writer')
    parser.add_argument('--name', type=str, help='Parent name')
    parser.add_argument('--age', type=int, help='Parent age')
    args = parser.parse_args()

    main(args.name, args.age)