from datetime import datetime
from pydantic import BaseModel
from typing import List


class Child(BaseModel):
    name: str
    age: int
    created_at: datetime = datetime.now()


class Parent(BaseModel):
    name: str
    age: int
    child: List[Child] = []
