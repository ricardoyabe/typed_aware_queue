"""Queue implementations"""
import json
from abc import ABC, abstractmethod
from typing import Callable, Dict, Generic, List, Optional, Type, TypeVar
import redis


T = TypeVar("T")


class AbstractQueue(ABC, Generic[T]):
    """The abstract queue"""

    @abstractmethod
    def __len__(self) -> int:
        """Return the size of this queue"""
        return 0

    @abstractmethod
    def rpop(self) -> T:
        """Remove and return the last item"""
        raise NotImplementedError

    @abstractmethod
    def lpop(self) -> T:
        """Remove and return the first item"""
        raise NotImplementedError

    @abstractmethod
    def rpush(self, value: T) -> None:
        """Push value onto the tail of the queue"""
        raise NotImplementedError

    @abstractmethod
    def lpush(self, value: T) -> None:
        """Push value onto the head of the queue"""


class ListQueue(AbstractQueue, Generic[T]):
    """Concrete Queue based on lists"""

    def __init__(self) -> None:
        self.l: List[T] = list()

    def __len__(self) -> int:
        return len(self.l)

    def rpop(self) -> T:
        return self.l.pop()

    def lpop(self) -> T:
        return self.l.pop(0)

    def rpush(self, value: T) -> None:
        self.l.append(value)

    def lpush(self, value: T) -> None:
        self.l.insert(0, value)


class RedisQueue(AbstractQueue):
    """Concrete Redis Queue"""

    def __init__(
        self, name: str, host: str, port: int, password: Optional[str] = None
    ) -> None:
        self.conn = redis.Redis(host=host, password=password, port=port)
        self.name = name

    def __len__(self) -> int:
        return self.conn.llen(self.name)

    def lpush(self, value: str) -> None:
        self.conn.lpush(self.name, value)

    def rpop(self) -> str:
        return self.conn.rpop(self.name)  # type: ignore

    def rpush(self, value: str) -> None:
        self.conn.rpush(self.name, value)

    def lpop(self) -> str:
        return self.conn.lpop(self.name)  # type: ignore


class TypeAwareQueue(AbstractQueue, Generic[T]):
    def __init__(self, actual_queue: AbstractQueue[str]) -> None:
        self.actual_queue = actual_queue
        self.serializer_mapping: Dict[str, Callable[[T], str]] = dict()
        self.deserializer_mapping: Dict[str, Callable[[str], T]] = dict()

    def add_encoder_decoder(
        self,
        clazz: Type[T],
        encoder_callback: Callable[[T], str],
        decoder_callback: Callable[[str], T],
    ) -> None:
        self.serializer_mapping[clazz.__name__] = encoder_callback
        self.deserializer_mapping[clazz.__name__] = decoder_callback

    def serialize_value(self, value: T) -> str:
        if type(value).__name__ in self.serializer_mapping:
            return self.serializer_mapping[type(value).__name__](value)
        raise AttributeError

    def deserialize_value(self, data: str) -> T:
        j = json.loads(data)
        class_name = j["type"]
        return self.deserializer_mapping[class_name](j["payload"])

    def __len__(self) -> int:
        return len(self.actual_queue)

    def lpush(self, value: T) -> None:
        payload = self.serialize_value(value)
        string_repr = json.dumps({"type": value.__class__.__name__, "payload": payload})
        self.actual_queue.lpush(string_repr)

    def rpush(self, value: T) -> None:
        payload = self.serialize_value(value)
        string_repr = json.dumps({"type": value.__class__.__name__, "payload": payload})
        self.actual_queue.rpush(string_repr)

    def lpop(self) -> T:
        return self.deserialize_value(self.actual_queue.lpop())

    def rpop(self) -> T:
        return self.deserialize_value(self.actual_queue.rpop())
