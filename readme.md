1. Run a local redis container:

```shell
docker run --name redis -p 6379:6379 redis
``` 

2. Open a terminal, create a Python 3.6 environment and from there run:

```shell
pip install -r requirements36.txt
```

3. Open another terminal, create a Python 3.9 environment and from there run:

```shell
pip install -r requirements39.txt
```

4. From the Python 3.6 environment, run:

```shell
python queue_writer.py --name 'from 3.6' --age 36
```

The output should be like this:

```shell
name='from 3.6' age=36 child=[Child(name='child name', age=3, created_at=datetime.datetime(2021, 7, 3, 22, 6, 27, 90737))]
```

5. From the Python 3.9 environment, run:

```shell
python queue_reader.py 
```

The output should be the same as the one from the previous step 

6. Repeat step 4 using the Python 3.9 environment, then repeat the step 5, using the Python 3.6 environment.

If everything goes well, it should be proven that the typed aware queue, running an embedded Redis client,
is compatible with both Python versions.
