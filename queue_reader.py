import json

from alana.domain import Parent
from alana.queue import RedisQueue, TypeAwareQueue

q = RedisQueue(name='queue', host='127.0.0.1', port=6379)
taq = TypeAwareQueue(q)
taq.add_encoder_decoder(
    clazz=Parent,
    encoder_callback=lambda obj: obj.json(),
    decoder_callback=lambda str_rep: Parent(**json.loads(str_rep))
)


def main() -> None:
    obj = taq.rpop()
    assert type(obj) == Parent
    print(obj)


if __name__ == '__main__':
    main()